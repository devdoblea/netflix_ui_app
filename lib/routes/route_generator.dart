import 'package:flutter/material.dart';
import 'package:netflix_ui_app/main.dart';
import 'package:netflix_ui_app/models/movies_model.dart';
import 'package:netflix_ui_app/screens/home_screen.dart';
import 'package:netflix_ui_app/screens/movie_screen.dart';

Widget makeRoute({ 
	@required BuildContext context,
	@required String routeName, 
	Object arguments }) {

		final Widget child = _buildRoute(
			context: context, 
			routeName: routeName, 
			arguments: arguments
		);

		return child;
}

Widget _buildRoute({ 
	@required BuildContext context,
	@required String routeName, 
	Object arguments }) {

		switch(routeName) {
			
			case '/':
				// Retornamos una ruta a la pagina principal
				return HomeScreen();
			
			case '/movies':
				// Si el dato no es String entonecs retorno una lista
				Movie movies = arguments as Movie;
				return MovieScreen(movie: movies);

			default:
				// si args no es del tipo correcto retorna un error_page
				// tambien se puede disparar un Exception en modo desarrolllo
				return ErrorRoute();

		}
}

class ErrorRoute extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text('Error'),
			),
			body: Center(
				child:Text('ERROR RUTA NO DEFINIDA'),
			),
		);
	}
}