import 'package:flutter/material.dart';
import 'package:netflix_ui_app/routes/route_generator.dart';
import 'package:netflix_ui_app/screens/home_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    	debugShowCheckedModeBanner: false,
      title: 'NetFlix Ui Redesign in Flutter',
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
      	return MaterialPageRoute(
					builder: (BuildContext context) => makeRoute(
						context: context,
						routeName: settings.name,
						arguments: settings.arguments,
					),
					maintainState: true,
					fullscreenDialog: false,
				);
      },
      home: HomeScreen(),
    );
  }
}
