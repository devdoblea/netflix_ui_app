Un REDISEÑO de la aplicacion para NETFLIX y para la navegacion entre paginas
se usó un Generador de Rutas para manejar las rutas con Navigator pushNamed

De aqui saque el ejemplo:
https://github.com/MarcusNg/flutter_netflix_ui_redesign

... y aqui el video:
https://www.youtube.com/watch?v=sgfMdhV4HQI

Y Asi quedó:

![](assets/Screenshot_2019-12-14-17-48-44.png)
![](assets/Screenshot_2019-12-14-17-48-58.png)
![](assets/Screenshot_2019-12-14-17-49-09.png)


# netflix_ui_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
